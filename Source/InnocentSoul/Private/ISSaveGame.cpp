// Copyright 2021. ICFHAG Studio. All Right Reserved


#include "ISSaveGame.h"
#include "Kismet/GameplayStatics.h"

bool UISSaveGame::LoadCustomInt(FString FieldName, int32& Value) const
{
	const int32* ValuePointer = ISCustomIntData.Find(FieldName);
	if (ValuePointer != nullptr)
	{
		Value = *ValuePointer;
		return true;
	}
	return false;
}

void UISSaveGame::SaveCustomInt(FString FieldName, int32 Value)
{
	ISCustomIntData.FindOrAdd(FieldName) = Value;
}

void UISSaveGame::ClearCustomInt(FString FieldName)
{
	ISCustomIntData.Remove(FieldName);
}
