// Fill out your copyright notice in the Description page of Project Settings.


#include "Actor/PlaygroundBorder.h"
#include "Components/BoxComponent.h"

APlaygroundBorder::APlaygroundBorder()
{
	Trigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	SetRootComponent(Trigger);
	Trigger->SetCollisionProfileName("OverlapAll");

}

void APlaygroundBorder::NotifyActorEndOverlap(AActor * OtherActor)
{
	Super::NotifyActorEndOverlap(OtherActor);
	OtherActor->Destroy();
}

