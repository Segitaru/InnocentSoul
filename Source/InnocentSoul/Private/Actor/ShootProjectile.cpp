// Fill out your copyright notice in the Description page of Project Settings.


#include "Actor/ShootProjectile.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/DamageType.h"
#include "GameFramework/Pawn.h"
#include "Components/ShootComponent.h"
#include "ISGameModeBase.h"
#include "Pawn/EnemyPawn.h"


AShootProjectile::AShootProjectile() :PrejectileSpeed(200.f)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Collision = CreateDefaultSubobject<USphereComponent>(TEXT("ProjectileCollision"));
	SetRootComponent(Collision);
	Collision->SetCollisionResponseToAllChannels(ECR_Ignore);
	Collision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	Collision->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Overlap);
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMesh"));
	Mesh->SetupAttachment(RootComponent);
	Mesh->SetCollisionProfileName("NoCollision");
}

// Called when the game starts or when spawned
void AShootProjectile::BeginPlay()
{
	Super::BeginPlay();
	Owner = GetOwner();
	Collision->OnComponentBeginOverlap.AddDynamic(this, &AShootProjectile::OnProjectileOverlap);

}

void AShootProjectile::OnProjectileOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 BodyIndex, bool Sweep, const FHitResult& Hit)
{
	APawn* OtherPawn = Cast<APawn>(OtherActor);
	if (!OtherActor || !OtherPawn) return;
	if (!Owner) return;
	APawn* PawnOwner = Cast<APawn>(GetOwner());
	if (!PawnOwner) return;
	AController* Instigators = PawnOwner->GetController();
	if (OtherActor == Owner) return;
	if (Owner->GetClass() == OtherActor->GetClass()) return;
	if (!PawnOwner->GetController() && !OtherPawn->GetController()) return;
	UGameplayStatics::ApplyDamage(OtherActor, Damage, Instigators, this, UDamageType::StaticClass());
	Destroy();
}

// Called every frame
void AShootProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AddActorLocalOffset(FVector(PrejectileSpeed*DeltaTime, 0.f, 0.f));

}
