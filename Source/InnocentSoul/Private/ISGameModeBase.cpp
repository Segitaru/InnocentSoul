// Fill out your copyright notice in the Description page of Project Settings.


#include "ISGameModeBase.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"
#include "Pawn/PlayerPawn.h"

AISGameModeBase::AISGameModeBase() :PlayerRecoverTime(2), PeriodUPDifficul(1.f), CurrentShootLevel(-1)
{
	EnemySpawnComponent = CreateDefaultSubobject<UEnemySpawnComponent>(TEXT("EnemySpawnComponent"));
	GameHealthComponent = CreateDefaultSubobject<UGameHealthComponent>(TEXT("GameHealth"));


}

void AISGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	GameHealthComponent->HelthsEndet.AddDynamic(this, &AISGameModeBase::EndGame);
	PlayerPawn = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (!PlayerPawn) return;
	ChangeShootLevel(true);
	PlayerPawn->PawnDamaged.AddDynamic(this, &AISGameModeBase::DestroyPawn);
	GetWorld()->GetTimerManager().SetTimer(DifficulTimer, this, &AISGameModeBase::IncreaseDifficulGame, PeriodUPDifficul, true);
}

void AISGameModeBase::DestroyPawn_Implementation()
{
	if (!PlayerPawn) return;
	PlayerPawn->ExploudPawn();
	GameHealthComponent->ChangeHealth(-1);
	ChangeShootLevel(false);
	if (!IsGameOver)
		GetWorld()->GetTimerManager().SetTimer(RecoverTimer, this, &AISGameModeBase::RecoverPawn, PlayerRecoverTime, false);
}

void AISGameModeBase::RecoverPawn_Implementation()
{
	if (!PlayerPawn) return;
	PlayerPawn->RecoverPawn();
}

void AISGameModeBase::EndGame()
{

	IsGameOver = true;
	EnemySpawnComponent->SetActive(false);
	GameOver.Broadcast();
	UGameplayStatics::GetPlayerPawn(this, 0)->Destroy();
	//APlayerController* Controller = UGameplayStatics::GetPlayerController(this, 0);
	//SetPause(Controller, false);
}

void AISGameModeBase::AddPoint(int Value)
{
	GamePoints += Value;
}

void AISGameModeBase::IncreaseDifficulGame()
{
	EnemySpawnComponent->CoefficientDifficul = FMath::Max(EnemySpawnComponent->CoefficientDifficul * 0.95, 0.4);
}

bool AISGameModeBase::ChangeShootLevel(bool Up)
{
	PlayerPawn = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (!PlayerPawn) return false;
	int NewLevel = FMath::Clamp(CurrentShootLevel + (Up ? 1 : -1), 0, ShootInfoLevel.Num() - 1);
	if (NewLevel == CurrentShootLevel) return false;
	CurrentShootLevel = NewLevel;
	PlayerPawn->ShootComponent->ShootInfos = ShootInfoLevel[CurrentShootLevel].ShootInfos;
	PlayerPawn->ShootComponent->ShootPeriod = ShootInfoLevel[CurrentShootLevel].ShootPeriod;
	return true;
}

// From Match3
bool AISGameModeBase::IsGameActive() const
{
	// Game is active whenever time hasn't run out or the timer is paused.
	FTimerManager& WorldTimerManager = GetWorldTimerManager();
	return (WorldTimerManager.IsTimerActive(GameOverTimer) || WorldTimerManager.IsTimerPaused(GameOverTimer));
}
void AISGameModeBase::PauseGameTimer(bool bPause)
{
	if (bPause)
	{
		GetWorldTimerManager().PauseTimer(GameOverTimer);
	}
	else
	{
		GetWorldTimerManager().UnPauseTimer(GameOverTimer);
	}
}