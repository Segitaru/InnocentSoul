// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/EnemyPawn.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/DamageType.h"
#include "Pawn/PlayerPawn.h"
#include "ISGameModeBase.h"
#include "Engine/World.h"


// Sets default values
AEnemyPawn::AEnemyPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("PawnCollision"));
	SetRootComponent(PawnCollision);
	PawnCollision->SetCollisionProfileName("Pawn");
	PawnMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PawnMesh"));
	PawnMesh->SetupAttachment(RootComponent);
	PawnMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ShootComponent = CreateDefaultSubobject<UShootComponent>(TEXT("ShootComponent"));
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
}

// Called when the game starts or when spawned
void AEnemyPawn::BeginPlay()
{
	Super::BeginPlay();
	HealthComponent->OnHealthEndet.AddDynamic(this, &AEnemyPawn::KillPawn);
	OnActorBeginOverlap.AddDynamic(this, &AEnemyPawn::OnActorOverlap);
}

void AEnemyPawn::SpawnBonus()
{
	FRandomStream ChanceSpawn;
	ChanceSpawn.GenerateNewSeed();

	FActorSpawnParameters SpawnParametrs;
	SpawnParametrs.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	for (FBonusInfo Bonus : PosibleBonus)
	{
		if (ChanceSpawn.RandRange(0.f, 100.f) < Bonus.ChanceSpawn)
		{
			GetWorld()->SpawnActor<ABonus>(Bonus.BonusClass, GetActorLocation(), FRotator(0.f), SpawnParametrs);
		}
	}
}

void AEnemyPawn::DestroyPawn()
{
	Destroy();
	if (!DestroyParticle) return;
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DestroyParticle, GetActorTransform(), true);
}
void AEnemyPawn::KillPawn()
{
	AISGameModeBase* GameMode = Cast<AISGameModeBase>(UGameplayStatics::GetGameMode(this));
	if (GameMode) GameMode->AddPoint(CostEnemy);
	DestroyPawn();
	SpawnBonus();
}
// Called every frame
void AEnemyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	float WorldMoveOffset = -250.f;
	AddActorWorldOffset(FVector(WorldMoveOffset *DeltaTime, 0.f, 0.f));
}



void AEnemyPawn::OnActorOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (OtherActor != UGameplayStatics::GetPlayerPawn(this, 0)) return;
	float AppliedDamage = UGameplayStatics::ApplyDamage(OtherActor, 1.f, GetController(), this, UDamageType::StaticClass());
	if (AppliedDamage > 0) DestroyPawn();
}