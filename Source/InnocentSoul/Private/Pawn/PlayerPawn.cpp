// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/PlayerPawn.h"
#include "Components/InputComponent.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"




// Sets default values
APlayerPawn::APlayerPawn() :MoveLimit(FVector2D(800.f, 1200.f)), TouchMoveSensivity(1.f), CanReceiveDamage(true)
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("PawnCollision"));
	SetRootComponent(PawnCollision);
	PawnCollision->SetCollisionProfileName("Pawn");
	PawnMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PawnMesh"));
	PawnMesh->SetupAttachment(RootComponent);
	PawnMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	ShootComponent = CreateDefaultSubobject<UShootComponent>(TEXT("ShootComponent"));

}


// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();

}

float APlayerPawn::TakeDamage(float Damage, const FDamageEvent& DamageEvent, AController * InstigatedBy, AActor * DamageCauser)
{
	if (!CanBeDamage()) return 0.f;

	Super::TakeDamage(Damage, DamageEvent, InstigatedBy, DamageCauser);
	PawnDamaged.Broadcast();
	return Damage;
}

void APlayerPawn::PossessedBy(AController * NewController)
{
	PlayerController = Cast<APlayerController>(NewController);
}


bool APlayerPawn::CanBeDamage_Implementation()
{
	return CanReceiveDamage;
}

void APlayerPawn::RecoverPawn_Implementation()
{
	//SetActorEnableCollision(true);
	if (!PawnCollision) return;
	PawnCollision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	PawnCollision->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Overlap);
	PawnCollision->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Overlap);
	if (!ShootComponent) return;
	ShootComponent->StartShooting();
	for (UActorComponent* Component : GetComponentsByClass(UParticleSystemComponent::StaticClass()))
	{
		Component->Activate(true);
	}
}

void APlayerPawn::ExploudPawn_Implementation()
{
	//SetActorEnableCollision(false);
	ShootComponent->StopShooting();
	PawnCollision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	PawnCollision->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Ignore);
	PawnCollision->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Ignore);
	for (UActorComponent* Component:GetComponentsByClass(UParticleSystemComponent::StaticClass()))		
	{
		Component->Deactivate();
	}
	if (!DestroyParticle) return;
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DestroyParticle, GetActorTransform(), true);
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindTouch(IE_Pressed, this, &APlayerPawn::OnTouchPress);
	//InputComponent->BindTouch(IE_Released, this, &APlayerPawn::OnTouchReleased);
	InputComponent->BindTouch(IE_Repeat, this, &APlayerPawn::OnTouchMove);
}


void APlayerPawn::OnTouchMove(ETouchIndex::Type FingerIndex, FVector Location)
{

	FVector2D TouchDeltaMove = FVector2D(TouchLocation.X - Location.X, TouchLocation.Y - Location.Y);
	TouchDeltaMove = TouchDeltaMove * TouchMoveSensivity;

	FVector NewLocation = GetActorLocation();
	NewLocation.X = FMath::Clamp(NewLocation.X + TouchDeltaMove.Y, -MoveLimit.Y, MoveLimit.Y);
	NewLocation.Y = FMath::Clamp(NewLocation.Y + TouchDeltaMove.X*-1.f, -MoveLimit.X, MoveLimit.X);

	SetActorLocation(NewLocation);
	TouchLocation = FVector2D(Location.X, Location.Y);
}

void APlayerPawn::OnTouchPress(ETouchIndex::Type FingerIndex, FVector Location)
{
	TouchLocation = FVector2D(Location.X, Location.Y);
}


