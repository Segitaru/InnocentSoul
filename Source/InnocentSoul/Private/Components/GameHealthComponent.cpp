// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/GameHealthComponent.h"
#include "GameFramework/Pawn.h"
#include "Kismet/GameplayStatics.h"

UGameHealthComponent::UGameHealthComponent() :Healths(3)
{

}


// Called when the game starts
void UGameHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
	//PlayerPawn->OnTakeAnyDamage.AddDynamic(this, &UGameHealthComponent::OnPlayerDamaged);
}

void UGameHealthComponent::ChangeHealth(int ByValue)
{
	Healths += ByValue;
	HealthsChange.Broadcast(ByValue);
	if (Healths <= 0)
	{
		HelthsEndet.Broadcast();
	}
}

int UGameHealthComponent::GetHealth()
{
	return Healths;
}
