// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/EnemySpawnComponent.h"
#include "ISGameModeBase.h"
#include "Kismet/GameplayStatics.h"

UEnemySpawnComponent::UEnemySpawnComponent() :StageDelayMin(1.f), StageDelayMax(2.f), CoefficientDifficul(2.f)
{
}


// Called when the game starts
void UEnemySpawnComponent::BeginPlay()
{
	Super::BeginPlay();
	Random.GenerateNewSeed();
	StartSpawnStage();
}

void UEnemySpawnComponent::StartSpawnStage()
{
	SpawnStage = SpawnStages[Random.RandRange(0, SpawnStages.Num() - 1)];
	EnemySpawned = 0;
	SpawnEnemy();
	float StageTimeDelay = Random.RandRange(StageDelayMin, StageDelayMax)* CoefficientDifficul;

	GetWorld()->GetTimerManager().SetTimer(ChangeStageTimer, this, &UEnemySpawnComponent::StartSpawnStage, StageTimeDelay, false);
}

void UEnemySpawnComponent::SpawnEnemy()
{
	FActorSpawnParameters SpawnParametr;
	GetWorld()->SpawnActor<AEnemyPawn>(SpawnStage.EnemyClass, SpawnStage.SpawnTransform, SpawnParametr);
	EnemySpawned++;
	if (EnemySpawned < SpawnStage.NumOfEnemy)
	{
		GetWorld()->GetTimerManager().SetTimer(EnemySpawTimer, this, &UEnemySpawnComponent::SpawnEnemy, SpawnStage.DelaySpawn, false);
	}

}

void UEnemySpawnComponent::Deactivate()
{
	GetWorld()->GetTimerManager().ClearTimer(ChangeStageTimer);

	GetWorld()->GetTimerManager().ClearTimer(EnemySpawTimer);

}

