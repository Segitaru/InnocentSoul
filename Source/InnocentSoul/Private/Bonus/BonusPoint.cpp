// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonus/BonusPoint.h"
#include "Kismet/GameplayStatics.h"
#include "ISGameModeBase.h"

void ABonusPoint::BonusCollected_Implementation()
{
	AISGameModeBase* GameMode = Cast<AISGameModeBase>(UGameplayStatics::GetGameMode(this));
	if (GameMode) GameMode->AddPoint(CostBonus);
	Super::BonusCollected_Implementation();
}