// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonus/BonusActor/PawnShield.h"

#include "Pawn/PlayerPawn.h"
#include "TimerManager.h"
#include "Engine/World.h"

APawnShield::APawnShield() :ShieldDuration(3)
{


}

void APawnShield::ActivateShield(APlayerPawn * PlayerPawn)
{
	if (!PlayerPawn) { Destroy(); return; }
	PlayerPawnShield = PlayerPawn;
	PlayerPawn->CanReceiveDamage = false;
	FAttachmentTransformRules Rulse = FAttachmentTransformRules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, false);
	AttachToActor(PlayerPawn, Rulse);
	PlayerPawn->PawnCollision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	PlayerPawn->PawnCollision->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Ignore);
	GetWorld()->GetTimerManager().SetTimer(ShieldTimer, this, &APawnShield::DeactivateShield, ShieldDuration, false);
}

void APawnShield::DeactivateShield()
{
	if (!PlayerPawnShield) return;
	PlayerPawnShield->CanReceiveDamage = true;
	PlayerPawnShield->PawnCollision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	PlayerPawnShield->PawnCollision->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Overlap);
	Destroy();
}

