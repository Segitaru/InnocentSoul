// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonus/BonusShield.h"
#include "Kismet/GameplayStatics.h"
#include "Pawn/PlayerPawn.h"
#include "Engine/World.h"
#include "Bonus/BonusActor/PawnShield.h"

void ABonusShield::BonusCollected_Implementation()
{
	APawn* Pawn = UGameplayStatics::GetPlayerPawn(this, 0);
	if (!Pawn) return;
	APlayerPawn* PlayerPawn = Cast<APlayerPawn>(Pawn);
	if (!PlayerPawn || !PlayerPawn->CanReceiveDamage) return;
	FActorSpawnParameters SpawnParam;
	SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	APawnShield* Shield = GetWorld()->SpawnActor<APawnShield>(ShieldClass, SpawnParam);
	if(Shield) Shield->ActivateShield(PlayerPawn);
	Super::BonusCollected_Implementation();
}
