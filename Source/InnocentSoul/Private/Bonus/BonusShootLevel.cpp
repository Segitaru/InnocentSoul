// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonus/BonusShootLevel.h"
#include "Kismet/GameplayStatics.h"
#include "ISGameModeBase.h"

void ABonusShootLevel::BonusCollected_Implementation()
{
	AISGameModeBase* GameMode = Cast<AISGameModeBase>(UGameplayStatics::GetGameMode(this));
	if (!GameMode) return;

	GameMode->ChangeShootLevel(true);
	Super::BonusCollected_Implementation();
}
