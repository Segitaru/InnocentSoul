// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonus/Bonus.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Pawn/PlayerPawn.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABonus::ABonus()
{
	PrimaryActorTick.bCanEverTick = true;
	Collision = CreateDefaultSubobject<USphereComponent>(TEXT("BonusCollision"));
	SetRootComponent(Collision);
	Collision->SetCollisionObjectType(ECC_WorldDynamic);
	Collision->SetCollisionResponseToAllChannels(ECR_Ignore);
	Collision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	Collision->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Overlap);
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BonusMesh"));
	Mesh->SetupAttachment(RootComponent);
	Mesh->SetCollisionProfileName("NoCollision");
}



void ABonus::NotifyActorBeginOverlap(AActor * OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);
	if (!OtherActor) return;
	if (!Cast<APlayerPawn>(OtherActor)) return;
	BonusCollected();
}

void ABonus::BonusCollected_Implementation()
{
	Destroy();
	if (!DestroyParticle) return;
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DestroyParticle, GetActorTransform(), true);
}

void ABonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	float WorldMoveOffset = -250.f;
	AddActorWorldOffset(FVector(WorldMoveOffset *DeltaTime, 0.f, 0.f));
}

