// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Components/EnemySpawnComponent.h"
#include "Components/GameHealthComponent.h"
#include "Components/ShootComponent.h"
#include "ISGameModeBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGameOverEvent);
USTRUCT(BlueprintType)
struct FShootInfoLevel
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
		TArray<FShootInfo> ShootInfos;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
		float ShootPeriod;
};
UCLASS()
class INNOCENTSOUL_API AISGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
		AISGameModeBase();
	virtual void BeginPlay() override;
protected:
	FTimerHandle RecoverTimer;
	FTimerHandle DifficulTimer;
	UFUNCTION(BlueprintNativeEvent, Category = "Game")
		void DestroyPawn();
	void DestroyPawn_Implementation();
	UFUNCTION(BlueprintNativeEvent, Category = "Game")
		void RecoverPawn();
	void RecoverPawn_Implementation();

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Enemy")
		UEnemySpawnComponent* EnemySpawnComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "GameHealth")
		UGameHealthComponent* GameHealthComponent;
	UFUNCTION(BlueprintCallable, Category = "Game")
		void EndGame();
	UFUNCTION(BlueprintCallable, Category = "Game")
		void AddPoint(int Value);

	UFUNCTION(BlueprintCallable, Category = "Game")
		void IncreaseDifficulGame();

	UPROPERTY(BlueprintAssignable, Category = "GameHealth")
		FGameOverEvent GameOver;
	UPROPERTY(BlueprintReadWrite, Category = "GameHealth")
		float PlayerRecoverTime;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game")
		float PeriodUPDifficul;
	UPROPERTY(BlueprintReadOnly, Category = "Game")
		class APlayerPawn* PlayerPawn;
		bool IsGameOver;
	UPROPERTY(BlueprintReadOnly, Category = "Game")
		int GamePoints;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Shooting")
		TArray<FShootInfoLevel> ShootInfoLevel;
	UPROPERTY(BlueprintReadOnly, Category = "Shooting")
		int CurrentShootLevel;
	UFUNCTION(BlueprintCallable, Category = "Game")
		bool ChangeShootLevel(bool Up);

	//From Match 3
	FTimerHandle GameOverTimer;
	/** Function to identify whether or not game is currently being played. */
	UFUNCTION(BlueprintCallable, Category = "Game")
		bool IsGameActive() const;

	/** Function to pause the game timer (when things are moving). */
	UFUNCTION(BlueprintCallable, Category = "Game")
		void PauseGameTimer(bool bPause);
};