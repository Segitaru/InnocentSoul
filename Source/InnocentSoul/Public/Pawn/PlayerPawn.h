// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/ShootComponent.h"
#include "PlayerPawn.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPawnDamageEvent);

UCLASS()
class INNOCENTSOUL_API APlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawn();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual float TakeDamage(float Damage, const FDamageEvent& DamageEvent, AController* InstigatedBy, AActor* DamageCauser) override;
	void OnTouchMove(ETouchIndex::Type FingerIndex, FVector Location);
	void OnTouchPress(ETouchIndex::Type FingerIndex, FVector Location);
	void PossessedBy(AController* NewController) override;
	APlayerController* PlayerController;
	FVector2D MoveLimit;

private:
	FVector2D TouchLocation;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pawn")
		UBoxComponent* PawnCollision;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pawn")
		UStaticMeshComponent* PawnMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pawn")
		UCameraComponent* Camera;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pawn")
		UShootComponent* ShootComponent;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Visual")
		UParticleSystem* DestroyParticle;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Controls")
		float TouchMoveSensivity;
	UPROPERTY(BlueprintReadWrite, Category = "Healths")
		bool CanReceiveDamage;
	UPROPERTY(BlueprintAssignable, Category = "Healths")
		FPawnDamageEvent PawnDamaged;
	UFUNCTION(BlueprintNativeEvent, Category = "Healths")
		void RecoverPawn();
	void RecoverPawn_Implementation();
	UFUNCTION(BlueprintNativeEvent, Category = "Healths")
		void ExploudPawn();
	void ExploudPawn_Implementation();
	UFUNCTION(BlueprintNativeEvent, Category = "Healths")
		bool CanBeDamage();
	bool CanBeDamage_Implementation();
	
};

