// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PawnShield.generated.h"

UCLASS()
class INNOCENTSOUL_API APawnShield : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APawnShield();
	UFUNCTION(BlueprintCallable, Category = "Shield")
		void ActivateShield(APlayerPawn* PlayerPawn);
	UFUNCTION(BlueprintCallable, Category = "Shield")
		void DeactivateShield();
protected:
	FTimerHandle ShieldTimer;
	class APlayerPawn* PlayerPawnShield;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldDuration;
};