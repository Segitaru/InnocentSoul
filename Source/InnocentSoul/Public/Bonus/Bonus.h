// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bonus.generated.h"

UCLASS(Blueprintable)
class INNOCENTSOUL_API ABonus : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABonus();

protected:
	virtual void Tick(float DeltaTime) override;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	UFUNCTION(BlueprintNativeEvent)
		void BonusCollected();
	virtual void BonusCollected_Implementation();
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Shoting")
		class USphereComponent* Collision;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Shoting")
		class UStaticMeshComponent* Mesh;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Visual")
		UParticleSystem* DestroyParticle;
};