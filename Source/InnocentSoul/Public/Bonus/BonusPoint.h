// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Bonus/Bonus.h"
#include "BonusPoint.generated.h"

/**
 * 
 */
UCLASS()
class INNOCENTSOUL_API ABonusPoint : public ABonus
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health")
		int CostBonus;
protected:
	virtual void BonusCollected_Implementation() override;
};
