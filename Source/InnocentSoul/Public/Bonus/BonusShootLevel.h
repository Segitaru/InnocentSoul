// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Bonus/Bonus.h"
#include "BonusShootLevel.generated.h"

/**
 * 
 */
UCLASS()
class INNOCENTSOUL_API ABonusShootLevel : public ABonus
{
	GENERATED_BODY()
protected:
	virtual void BonusCollected_Implementation() override;
};
