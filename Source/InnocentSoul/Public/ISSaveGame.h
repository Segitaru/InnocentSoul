// Copyright 2021. ICFHAG Studio. All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "ISSaveGame.generated.h"

USTRUCT(BlueprintType)
struct FISSaveData
{
	GENERATED_USTRUCT_BODY()

		
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 TopScore;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 Gold;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 Gems;
};
UCLASS()
class INNOCENTSOUL_API UISSaveGame : public USaveGame
{
	GENERATED_BODY()
public:
	UPROPERTY()
		TMap<FString, FISSaveData> ISSaveData;
	/** Load the int32 value associated with the requested variable. */
	bool LoadCustomInt(FString FieldName, int32& Value) const;

	/** Create a variable in the saved game and associate the provided integer value with it. */
	void SaveCustomInt(FString FieldName, int32 Value);

	/** Erase a variable from the saved game. */
	void ClearCustomInt(FString FieldName);

protected:
	UPROPERTY()
		TMap<FString, int32> ISCustomIntData;
};