// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "ShootProjectile.generated.h"

UCLASS()
class INNOCENTSOUL_API AShootProjectile : public AActor
{
	GENERATED_BODY()

public:
	AShootProjectile();
protected:
	virtual void BeginPlay() override;
	AActor* Owner;
	UFUNCTION()
		void OnProjectileOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 BodyIndex, bool Sweep, const FHitResult& Hit);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Shoting")
		USphereComponent* Collision;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Shoting")
		UStaticMeshComponent* Mesh;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Shoting")
		float PrejectileSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Shoting")
		float Damage;


};
