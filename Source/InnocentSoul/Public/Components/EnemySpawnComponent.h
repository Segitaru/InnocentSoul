// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Pawn/EnemyPawn.h"
#include "EnemySpawnComponent.generated.h"

USTRUCT(BlueprintType)
struct FEnemySpawnInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy")
		TSubclassOf<AEnemyPawn> EnemyClass = AEnemyPawn::StaticClass();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy")
		FTransform SpawnTransform;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy")
		int NumOfEnemy;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy")
		float DelaySpawn;
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class INNOCENTSOUL_API UEnemySpawnComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UEnemySpawnComponent();

protected:

	// Called when the game starts
	virtual void BeginPlay() override;
	void StartSpawnStage();
	void SpawnEnemy();
	FEnemySpawnInfo SpawnStage;
	int EnemySpawned;
	FTimerHandle ChangeStageTimer;
	FTimerHandle EnemySpawTimer;
	FRandomStream Random;
	virtual void Deactivate() override;

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Enemy")
		TArray<FEnemySpawnInfo> SpawnStages;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Enemy")
		float StageDelayMin;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Enemy")
		float StageDelayMax;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Enemy")
		float CoefficientDifficul;

};