// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHealthsEndEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHealthsChangeEvent, int, ChangeValue);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class INNOCENTSOUL_API UGameHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UGameHealthComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	UPROPERTY(VisibleAnywhere, Category = "GameHealth")
		int Healths;
	//UFUNCTION()
	//	void OnPlayerDamaged(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* Instigator, AActor* DamageCauser);
public:
	UFUNCTION(BlueprintCallable, Category = "GameHealth")
		void ChangeHealth(int ByValue);
	UFUNCTION(BlueprintPure, Category = "GameHealth")
		int GetHealth();
	UPROPERTY(BlueprintAssignable, Category = "GameHealth")
		FHealthsEndEvent HelthsEndet;
	UPROPERTY(BlueprintAssignable, Category = "GameHealth")
		FHealthsChangeEvent HealthsChange;
};