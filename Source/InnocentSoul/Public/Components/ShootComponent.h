// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Actor/ShootProjectile.h"
#include "ShootComponent.generated.h"


USTRUCT(BlueprintType)
struct FShootInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
		TSubclassOf<AShootProjectile> ProjectileClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
		FVector ShootOfset;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
		float Angle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
		float Damage;
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class INNOCENTSOUL_API UShootComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UShootComponent();
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Shooting")
		float ShootPeriod;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Shooting")
		TArray<FShootInfo> ShootInfos;
	AActor* OwnerComponent;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	FTimerHandle ShootingTimer;
	void Shoot();


public:
	UFUNCTION(BlueprintCallable, Category = "Shooting")
		void StartShooting();
	UFUNCTION(BlueprintCallable, Category = "Shooting")
		void StopShooting();
};