// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "ISGameModeBase.h"
#include "ISSaveGame.h"
#include "ISGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class INNOCENTSOUL_API UISGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	UISGameInstance();
	// ���������� ����� ��� �������.
	void Init() override;

	// ���������� ����� ��� ���������� ������.
	void Shutdown() override;

	// ��� ������� ������������ ��� ������������� ��� ��������� ����� ��� ����������. ������ ���������� ��� �������� ������, �������������� �����.
	void InitSaveGameSlot();

	// ��������� ������� ����������� ����, ���� ��� ����������. 
	bool FindSaveDataForLevel(UObject* WorldContextObject, FISSaveData& OutSaveData);

	// ��������� ���� ����. �������� ��� ������ ���������� ����.
	UFUNCTION(BlueprintCallable, Category = "Saved Game")
		void SaveGame();

	// ������� ���������������� ���������� int32 � ����� ����������� ����. � FieldName ������� �� �����������.
	UFUNCTION(BlueprintCallable, Category = "Saved Game")
		bool LoadCustomInt(FString FieldName, int32& Value);

	// ��������� ���������������� ���������� int32 � ����� ����������� ����. � FieldName ������� �� �����������.
	UFUNCTION(BlueprintCallable, Category = "Saved Game")
		void SaveCustomInt(FString FieldName, int32 Value);

	// ������� ���������������� ���������� int32 �� ����� ����������� ����. � FieldName ������� �� �����������.
	UFUNCTION(BlueprintCallable, Category = "Saved Game")
		void ClearCustomInt(FString FieldName);

	// �������� ��� �������� ����������� ������ ��� ������������� ������ Match3.
	void UpdateSave(UObject* WorldContextObject, FISSaveData& NewData);

	// ������� ��� ���������� ���������� ����� ���������� ����������� ���
	UFUNCTION(BlueprintImplementableEvent)
		void UpdateUIAfterSave();

	// ������������ ������ ���������� ���� ��� ���� ������� / �������. 
	UPROPERTY()
		class UISSaveGame* InstanceGameData;

	// ������ ���������� ��������� ��� ��������� �������������� ������������, ��������, ��� ����� ��� ������ ����� ������� ���� �������� ����.
	UFUNCTION(BlueprintCallable, Category = "Online")
		void RegisterOnlineID(FString NewOnlineID);

	// ����������, ����� ������������ ������ � ������� ��� ������� �� ��� �� ��������� ����������.
	UFUNCTION(BlueprintImplementableEvent, Category = "Online")
		void OnLoginChanged(bool bLoggingIn, int32 UserID, int32 UserIndex);

	// ����������, ����� ���������� ������ ��� ����� �� �������� ����.
	UFUNCTION(BlueprintImplementableEvent, Category = "Online")
		void OnEnteringForeground();

	// ����������, ����� ���������� ���������� ������� � ������� �����.
	UFUNCTION(BlueprintImplementableEvent, Category = "Online")
		void OnEnteringBackground();

	// ���������� ������� ��� �������� � �������� ��������� ������� ���� ���������
	void OnViewportResize_Internal(FViewport* Viewport, uint32 ID);

	// ������� Blueprint, ������������ ��� ������ �� ��������� ������� ������� ���������, ��������� FViewport * �� �������� ���������� ����� ������ Blueprint
	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
		void OnViewportResize();

	// ���������� ������� ��� �������� � ������������ �������� �������
	void OnUnexpectedPurchase_Internal(const FUniqueNetId& UserId);

	// ����������, ����� ���������� �������� ������� ������� ��� ������ �������.
	UFUNCTION(BlueprintImplementableEvent, Category = "Online")
		void OnUnexpectedPurchase();

protected:
	FString GetSaveSlotName() const;
	FString SaveGamePrefix;
	FString DefaultSaveGameSlot;

private:
	FDelegateHandle LoginChangedHandle;
	FDelegateHandle EnteringForegroundHandle;
	FDelegateHandle EnteringBackgroundHandle;
	FDelegateHandle ViewportHandle;
	FDelegateHandle UnexpectedPurchaseHandle;
};
