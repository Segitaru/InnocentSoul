// Copyright Epic Games, Inc. All Rights Reserved.

#include "InnocentSoul.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, InnocentSoul, "InnocentSoul" );
