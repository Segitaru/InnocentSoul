// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class InnocentSoul : ModuleRules
{
	public InnocentSoul(ReadOnlyTargetRules Target) : base(Target)
	{
        PrivatePCHHeaderFile = "InnocentSoul.h";

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "UMG"});

		PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
        PrivateDependencyModuleNames.AddRange(new string[] { "OnlineSubsystem", "OnlineSubsystemUtils" });

        DynamicallyLoadedModuleNames.Add("OnlineSubsystemNull");

        if (Target.Platform == UnrealTargetPlatform.IOS)
        {
            PrivateDependencyModuleNames.AddRange(new string[] { "OnlineSubsystem", "OnlineSubsystemUtils" });
            DynamicallyLoadedModuleNames.Add("OnlineSubsystemFacebook");
            DynamicallyLoadedModuleNames.Add("OnlineSubsystemIOS");
            DynamicallyLoadedModuleNames.Add("IOSAdvertising");
        }
        else if (Target.Platform == UnrealTargetPlatform.Android)
        {
            PrivateDependencyModuleNames.AddRange(new string[] { "OnlineSubsystem", "OnlineSubsystemUtils" });
            DynamicallyLoadedModuleNames.Add("AndroidAdvertising");
            DynamicallyLoadedModuleNames.Add("OnlineSubsystemGooglePlay");
            //// Add UPL to add configrules.txt to our APK
            //string PluginPath = Utils.MakePathRelativeTo(ModuleDirectory, Target.RelativeEnginePath);
            //AdditionalPropertiesForReceipt.Add("AndroidPlugin", System.IO.Path.Combine(PluginPath, "AddRoundIcon_UPL.xml"));

        }
    }
}
